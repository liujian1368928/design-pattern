﻿using System;
using System.Collections;

namespace 多例模式
{
    class Program
    {
        static void Main(string[] args)
        {
            int ministerNum = 10; //10个大臣

            for (int i = 0; i < ministerNum; i++)
            {
                Emperor emperor = Emperor.getInstance();
                Console.WriteLine("第" + (i + 1) + "个大臣参拜的是：");
                emperor.emperorInfo();

            }
        }
    }

    /// <summary>
    /// 皇帝类
    /// </summary>
    public class Emperor
    {
        private static int maxNumOfEmperor = 2; //最多只能有连个皇帝

        private static ArrayList emperorInfoList = new ArrayList(maxNumOfEmperor); // 皇帝叫什么名字

        private static ArrayList emperorList = new ArrayList(maxNumOfEmperor); //装皇  帝的列表；

        private static int countNumOfEmperor = 0; //正在被人尊称的是那个皇帝
        static Emperor()
        {
            //把所有的皇帝都产生出来
            for (int i = 0; i < maxNumOfEmperor; i++)
            {
                emperorList.Add(new Emperor("皇" + (i + 1) + "帝"));
            }
        }

        //就这么多皇帝了，不允许再推举一个皇帝(new 一个皇帝）
        private Emperor()
        {
            //世俗和道德约束你，目的就是不让你产生第二个皇帝
        }

        private Emperor(String info)
        {
            emperorInfoList.Add(info);
        }

        public static Emperor getInstance()
        {

            Random random = new Random();
            countNumOfEmperor = random.Next(maxNumOfEmperor); //随机拉出一个皇帝， 只要是个精神领袖就成

            return (Emperor)emperorList[countNumOfEmperor];
        }

        //皇帝叫什么名字呀
        public void emperorInfo()
        {
            Console.WriteLine(emperorInfoList[countNumOfEmperor]);
        }
    }
}
