﻿using System;

namespace 策略模式
{
    class Program
    {
        static void Main(string[] args)
        {
            Content content;
            content = new Content(new operateOne());
            content.operate();
            content = new Content(new operateTwo());
            content.operate();
            content = new Content(new operateThree());
            content.operate();
            Console.ReadLine();
        }
    }

    ///锦囊接口
    public interface IStrategy
    {
        ///妙计内容方法
        void operate();
    }

    public class operateOne : IStrategy
    {

        public void operate()
        {
            Console.Write("我是妙计一");

        }
    }
    public class operateTwo : IStrategy
    {

        public void operate()
        {
            Console.Write("我是妙计二");

        }
    }

    public class operateThree : IStrategy
    {

        public void operate()
        {
            Console.Write("我是妙计三");

        }
    }

    public class Content
    {
        private IStrategy _iStrategy;
        public Content(IStrategy iStrategy)
        {

            _iStrategy = iStrategy;
        }

        public void operate()
        {
            _iStrategy.operate();
        }

    }
}
