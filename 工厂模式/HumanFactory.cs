﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 工厂模式
{
    public interface IHumanFactory
    {
        public IHuman CreateHuman();
    }

    public class CreateWhiteHumanFactory : IHumanFactory
    {
        public IHuman CreateHuman()
        {
            return new WhiterHuman();
        }
    }

}
