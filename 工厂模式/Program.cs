﻿using System;

namespace 工厂模式
{
    class Program
    {
        //给机器你自己创造
        static void Main(string[] args)
        {
            IHumanFactory _IHumanFactory = new CreateWhiteHumanFactory();
            IHuman human = _IHumanFactory.CreateHuman();
            human.Talk();
        }
    }
}
