﻿using System;

namespace 单例模式
{
    class Program
    {
        static void Main(string[] args)
        {
            Emperor em1 = Emperor.GetInstance();
            em1.EmperorInfo();
            Emperor em2 = Emperor.GetInstance();
            em2.EmperorInfo();
            Emperor em3 = Emperor.GetInstance();
            em3.EmperorInfo();
            Console.WriteLine(em1.GetHashCode() + "间隔" + em2.GetHashCode() + "间隔" + em3.GetHashCode());
            Console.ReadKey();
        }
    }

    public class Emperor
    {
        private static Emperor emperor = null;
        public Emperor()
        {
        }

        public static Emperor GetInstance()
        {
            if (emperor == null)
            {
                emperor = new Emperor();
            }
            return emperor;
        }

        public void EmperorInfo()
        {
            Console.WriteLine("我是皇帝啊啊啊啊");
        }

    }
}
