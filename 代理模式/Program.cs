﻿using System;

namespace 代理模式
{
    class Program//西门庆
    {
        static void Main(string[] args)
        {
            WangPo wangpo = new WangPo();
            wangpo.HappyWithMan();
            wangpo.MakeEaysWithMan();
            WangPo wangpo2 = new WangPo(new PanJinLian2());//传入所有实现women接口的对象都能被王婆锁调用
            wangpo2.HappyWithMan();
            wangpo2.MakeEaysWithMan();
        }
    }
    public interface Women
    {
        void HappyWithMan();
        void MakeEaysWithMan();
    }

    public class PanJinLian : Women
    {
        public void HappyWithMan()
        {
            Console.WriteLine("潘金莲和西门庆开心中");
        }

        public void MakeEaysWithMan()
        {
            Console.WriteLine("抛媚眼中");
        }
    }
    public class PanJinLian2 : Women
    {
        public void HappyWithMan()
        {
            Console.WriteLine("潘金莲2和西门庆开心中");
        }

        public void MakeEaysWithMan()
        {
            Console.WriteLine("潘金莲2抛媚眼中");
        }
    }

    public class PanJinLian3 : Women
    {
        public void HappyWithMan()
        {
            Console.WriteLine("潘金莲3和西门庆开心中");
        }

        public void MakeEaysWithMan()
        {
            Console.WriteLine("潘金莲3抛媚眼中");
        }
    }

    public class WangPo : Women
    {
        private Women woman;
        public WangPo()
        {
            woman = new PanJinLian();
        }

        public WangPo(Women woman) //接受所有女人的代理
        {
            this.woman = woman;
        }
        public void HappyWithMan()
        {
            woman.HappyWithMan();
        }

        public void MakeEaysWithMan()
        {
            woman.MakeEaysWithMan();
        }
    }
}
