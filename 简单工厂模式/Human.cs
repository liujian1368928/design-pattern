﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 简单工厂模式
{
    public interface IHuman
    {
        //首先定义什么是人类

        //人是愉快的，会笑的，本来是想用smile表示，想了一下laugh更合适，好长时间没有大笑了；
        public void laugh();

        //人类还会哭，代表痛苦
        public void cry();

        //人类会说话
        public void talk();

    }
    public class YellowHuman : IHuman
    {
        public void cry()
        {
            Console.WriteLine("黄色人类会哭");
        }
        public void laugh()
        {
            Console.WriteLine("黄色人类会大笑，幸福呀！");
        }
        public void talk()
        {
            Console.WriteLine("黄色人类会说话，一般说的都是双字节");
        }
    }

    public class WhiteHuman : IHuman
    {
        public void cry()
        {
            Console.WriteLine("白色人类会哭");
        }
        public void laugh()
        {
            Console.WriteLine("白色人类会大笑，侵略的笑声");
        }
        public void talk()
        {
            Console.WriteLine("白色人类会说话，一般都是但是单字节！");
        }
    }


    /**
    * @author cbf4Life cbf4life@126.com
    * I'm glad to share my knowledge with you all.
    * 黑色人类，记得中学学英语，老师说black man是侮辱人的意思，不懂，没跟老外说话
*/
    public class BlackHuman : IHuman
    {
        public void cry()
        {
            Console.WriteLine("黑人会哭");
        }
        public void laugh()
        {
            Console.WriteLine("黑人会笑");
        }
        public void talk()
        {
            Console.WriteLine("黑人可以说话，一般人听不懂");
        }
    }
}
