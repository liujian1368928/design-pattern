﻿using System;
using System.Collections.Generic;
using System.Text;

namespace 简单工厂模式
{
     public  class HumanFactory
    {
        public static IHuman createHuman(string name)
        {
            IHuman human = null; //定义一个类型的人类

            if (name.Equals("white"))
            {
                human = new WhiteHuman();
            }
            else if (name.Equals("black"))
            {
                human = new BlackHuman();
            }
            else if (name.Equals("yellow")) 
            {
                human = new YellowHuman();
            }
             return human;
        }
    }
}
