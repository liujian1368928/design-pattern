﻿using System;

namespace 简单工厂模式
{
    class Program
    {
        //给原料帮你弄出来
        static void Main(string[] args)
        {
           // Console.WriteLine("------------造出的第一批人是这样的：白人 ---------------- - ");

            IHuman whiteHuman = HumanFactory.createHuman("white");
            whiteHuman.GetType();
        }
   
    }


}
